package reconstructor

import (
	"testing"
	"math/rand"
	. "strconv"
)


const (
	longStr = `He that will give good words to thee will flatter
                   Beneath abhorring. What would you have, you curs,
                   That like nor peace nor war? the one affrights you,
                   The other makes you proud. He that trusts to you,
                   Where he should find you lions, finds you hares;
                   Where foxes, geese: you are no surer, no,
                   Than is the coal of fire upon the ice,
                   Or hailstone in the sun. Your virtue is
                   To make him worthy whose offence subdues him
                   And curse that justice did it.
                   Who deserves greatness
                   Deserves your hate; and your affections are
                   A sick man's appetite, who desires most that
                   Which would increase his evil. He that depends
                   Upon your favours swims with fins of lead
                   And hews down oaks with rushes. Hang ye! Trust Ye?
                   With every minute you do change a mind,
                   And call him noble that was now your hate,
                   Him vile that was your garland. What's the matter,
                   That in these several places of the city
                   You cry against the noble senate, who,
                   Under the gods, keep you in awe, which else
                   Would feed on one another? What's their seeking?`

	repeatingStr = "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789"
)

func printChunks(t *testing.T, chunks []*ItemChunk) {

	var str string

	for _, chunk := range chunks {
		str = str + "[ " + chunk.Id + ":" + Itoa(chunk.ChunkNum) +" ]"
	}

	t.Logf(str)
}

func shuffleChunks(chunks1, chunks2 []*ItemChunk) []*ItemChunk {
	allChunks := make([]*ItemChunk, len(chunks1) + len(chunks2))

	copy(allChunks, chunks1)
	copy(allChunks[len(chunks1):], chunks2)

	// Put all the chunks in one array to shuffle it to simulate the
	// chunks arriving in random order
	for i := range allChunks {
		j := rand.Intn(i + 1)
		allChunks[i], allChunks[j] = allChunks[j], allChunks[i]
	}

	return allChunks
}



func interleaveChunks(chunks1, chunks2 []*ItemChunk) []*ItemChunk {
	TotalLength := len(chunks1) + len(chunks2)
	allChunks := make([]*ItemChunk, TotalLength)

	min := func (x, y int) int {
		if x < y {
			return x
		} else {
			return y
		}
	}

	overlap := min(len(chunks1), len(chunks2))

	for i := 0; i < overlap; i++ {
		allChunks[2*i] = chunks1[i]
		allChunks[2*i+1] = chunks2[i]
	}

	// Copy remainder of chunks1 to the end of the array
	if len(chunks1) > len(chunks2) {
		copy(allChunks[2*overlap:],chunks1[overlap:])
	// Copy remainder of chunks2 to the end of the array
	} else if len(chunks1) < len(chunks2) {
		copy(allChunks[2*overlap:],chunks2[overlap:])
	} else { // len(chunks1) == len(chunks2)
		 // We're done
	}

	return allChunks
}

func checkRecovery(t *testing.T, r *Reconstructor, chunks []*ItemChunk, strId string, initialString string) {
	for _, chunk := range chunks {
		if r.AddChunk(chunk) {
			recoveredBytes, err := r.RetrieveItem(strId)
			if err != nil {
				t.Error("There was an error trying to recover " + strId)
				return
			}

			recoveredStr := string(recoveredBytes)
			if initialString == recoveredStr {
				t.Log(strId + " has been recovered")
			} else {
				t.Error(strId + " has not been recovered")
			}
		}
	}
}

func checkRecoveryWithTwoItemsMixed(t *testing.T, item1Id, item1Str, item2Id, item2Str string, mixingFunc func(chunk1, chunk2 []*ItemChunk) []*ItemChunk) {

	d := Deconstructor{}
	chunksLong := d.MakeChunks(item1Id, []byte(item1Str))

	chunksRepeating := d.MakeChunks(item2Id, []byte(item2Str))

	r := &Reconstructor{}

	allChunks := mixingFunc(chunksLong, chunksRepeating)

	t.Log("Printing the mixed chunks")
	printChunks(t, allChunks)

	for _, chunk := range allChunks {
		if r.AddChunk(chunk) {
			itemId := chunk.Id
			item, err := r.RetrieveItem(itemId)
			if err != nil {
				t.Fatalf("Error: %s", err.Error())
			}

			if itemId == item1Id {
				if string(item) == item1Str {
					t.Logf("The LongString has been retreived")
				} else {
					t.Errorf("The LongString has was not retreived successfully")
					t.Errorf("Expected %s", item1Str)
					t.Errorf("Got %s", string(item))
				}
			} else {
				if string(item) == item2Str {
					t.Logf("The RepeatingString has been retreived")
				} else {
					t.Errorf("The RepeatingString has was not retreived successfully")
					t.Errorf("Expected %s", item2Str)
					t.Errorf("Got %s", string(item))
				}
			}
		}
	}
}

func basicChunkTest(t *testing.T, strBytes []byte, strId string, totalChunks int) {

	t.Logf("Small string id: %s\n", strId)

	d := Deconstructor{}
	chunks := d.MakeChunks(strId, strBytes)

	if len(chunks) == totalChunks {
		t.Log("Small string has the right number of chunks")
	} else {
		t.Errorf("Expected %d chunks but it has this many chunks: %d", totalChunks, len(chunks))
	}

	chunk := chunks[0]
	
	if chunk.Id == strId {
		t.Logf("The one chunk has the right Id               : %s", strId)
	} else {
		t.Errorf("Expected Id %s but chunk has Id: %s", strId, chunk.Id)
	}

	if chunk.TotalChunks == totalChunks {
		t.Log("The one chunk has the right total chunks     : 1")
	} else {
		t.Errorf("Expected %d total chunks but it has total chunks: %d", totalChunks, chunk.TotalChunks)
	}

	if chunk.ChunkNum == 0 {
		t.Log("The one chunk has right position in the array: 0")
	} else {
		t.Errorf("Expected chunk in the 0 position but it has position: %d", chunk.ChunkNum)
	}
}

func TestSmallStringHasRightChunks(t *testing.T) {

	basicChunkTest(t, []byte("abc"), "SmallString", 1)
}

func TestEmptyStringHasRightChunks(t *testing.T) {

	basicChunkTest(t, []byte(""), "EmptyString", 1)
}

func TestLongStringHasRightChunks(t *testing.T) {

	basicChunkTest(t, []byte(repeatingStr), "LongString", len(repeatingStr)/MAX_CHUNK_SIZE + 1)
}

func basicRecoveryTest(t *testing.T, str, strId string) {

	strBytes := []byte(str)

	d := Deconstructor{}
	chunks := d.MakeChunks(strId, strBytes)

	r := &Reconstructor{}

	checkRecovery(t, r, chunks, strId, str)
}

func TestLongStringRecovery(t *testing.T) {

	basicRecoveryTest(t, longStr, "LongString")
}

func TestSmallStringRecovery(t *testing.T) {
	basicRecoveryTest(t, "abc", "SmallString")
}

func TestEmptyStringRecovery(t *testing.T) {
	basicRecoveryTest(t, "", "EmptyString")
}

func TestLongStringRearrangesToSameString(t *testing.T) {

	d := Deconstructor{}

	strId := "LongUnshuffledString"
	chunks := d.MakeChunks(strId, []byte(longStr))

	shuffledChunks := shuffleChunks(chunks, nil)

	r := &Reconstructor{}
	checkRecovery(t, r, shuffledChunks, strId, longStr)
}

func TestLongStringIsAddedTwice(t *testing.T) {

	d := Deconstructor{}

	strId := "LongRepeatedUnshuffledString"
	chunks := d.MakeChunks(strId, []byte(longStr))

	r := &Reconstructor{}
	TotalChunks := len(chunks)
	for i, chunk := range chunks {
		if r.AddChunk(chunk) {
			if i != TotalChunks-1 {
				t.Errorf("The string was reconstructed before all the pieces were in place. That's weird")
			}
		}
	}

	// Adding the chunks for the second time
	for _, chunk := range chunks {
		if r.AddChunk(chunk) == false {
			t.Errorf("The string should already be reconstructed by this point")
		}
	}

	recoveredBytes, err := r.RetrieveItem(strId)
	if err != nil {
		t.Error("There was an error trying to recover " + strId)
		return
	}

	recoveredStr := string(recoveredBytes)
	if longStr == recoveredStr {
		t.Logf("The string has been recovered")
	} else {
		t.Error("The reconstructed string is not the same as the original string")
	}
}


func TestInterleavingRecoveringStrings(t *testing.T) {

	checkRecoveryWithTwoItemsMixed(t, "LongString", longStr, "RepeatingString", repeatingStr, interleaveChunks)
}

func TestInterleavingandShufflingRecoveringStrings(t *testing.T) {

	checkRecoveryWithTwoItemsMixed(t, "LongString", longStr, "RepeatingString", repeatingStr, shuffleChunks)
}
