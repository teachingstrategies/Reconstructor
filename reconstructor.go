package reconstructor

import (
	_ "fmt"
	"errors"
)

type ItemChunk struct {
	Id            string   `json:"id"`
	TotalChunks   int      `json:"total_chunks"`
	ChunkNum      int      `json:"chunk_num"`
	Data          []byte   `json:"data"`
}

type ItemMetadata struct {
	Chunks        []*ItemChunk
	CurrentChunks int  
	TotalChunks   int
}

type Deconstructor struct {

}

// This interface documents the interface to the
// Deconstructor data type.
type Deconstruction interface {

	MakeChunks(id string, data []byte) []*ItemChunk
}


type Reconstructor map[string]*ItemMetadata

// This interface documents the interface to the
// Reconstructor data type. 
type Reconstruction interface {

	AddChunk(chunk *ItemChunk) bool
	RetrieveItem(id string) ([]byte, error)
	DeleteItem(id string)
}
	
const MAX_CHUNK_SIZE = 256

var (
	ErrNoItemAvailable   = errors.New("There is no item available with that ID")
	ErrItemChunksMissing = errors.New("There are pieces of this item missing")
)

func (d Deconstructor) MakeChunks(id string, data []byte) []*ItemChunk {

	if data == nil {
		return nil
	}

	TotalSize := len(data)
	TotalChunks := (TotalSize / MAX_CHUNK_SIZE) + 1
	chunks := make([]*ItemChunk, TotalChunks)

	var numBytes int
	bytesProcessed := 0
	chunkNum := 0
	for {
		if bytesProcessed + MAX_CHUNK_SIZE <= TotalSize {
			numBytes = MAX_CHUNK_SIZE
		} else {
			numBytes = TotalSize - bytesProcessed
		}

		chunk := &ItemChunk {
			Id: id,
			TotalChunks: TotalChunks,
			ChunkNum: chunkNum,
			Data: data[bytesProcessed:(bytesProcessed+numBytes)],
		}

		chunks[chunkNum] = chunk

		chunkNum++
		bytesProcessed += numBytes

		if bytesProcessed >= TotalSize {
			return chunks
		}
	}
}

func (r Reconstructor) AddChunk(chunk *ItemChunk) bool {

	if chunk == nil {
		return false
	}

	// The location within the collection of chunks 
	N := chunk.ChunkNum
	// Total number of chunks in the data
	TotalChunks := chunk.TotalChunks
	// If this is a new chunk then lets create the item meta data for this new item
	if itemMetadata, ok := r[chunk.Id]; !ok {

		itemMetadata = &ItemMetadata{}
		itemMetadata.CurrentChunks = 1
		itemMetadata.TotalChunks = TotalChunks

		itemMetadata.Chunks = make([]*ItemChunk, TotalChunks)
		itemMetadata.Chunks[N] = chunk

		r[chunk.Id] = itemMetadata

		// If there the data is only represented by
		// one chunk then we are done
		return TotalChunks == 1

		// We already have meta data for value this chunk
		// so we either store the new chunk, reject the new
		// chunk because we already have it or we tell the
		// user we're done because we already have all the
		// chunks
	} else {

		// If we already have all the chunks so we're done
		if itemMetadata.CurrentChunks == TotalChunks {
			return true
		}

		// We've already have this chunk so we're done
		if itemMetadata.Chunks[N] != nil {
			return false
		} 

		itemMetadata.Chunks[N] = chunk
		itemMetadata.CurrentChunks++

		// If we have all the chunks then we are done.
		// If not then we may still add chunks for this 'id'
		return itemMetadata.CurrentChunks == TotalChunks
	}
}

func (r Reconstructor) RetrieveItem(id string) ([]byte, error) {
	
	var Chunks []*ItemChunk

	// Make sure there is an item with the given 'id'
	if Chunks = r[id].Chunks; Chunks == nil {
		return nil, ErrNoItemAvailable
	}

	// Make sure we have all the pieces of data before trying to assemble them
	if len(Chunks) != r[id].CurrentChunks {
		return nil, ErrItemChunksMissing
	}

	TotalSize := 0
	for _, chunk := range Chunks {
		TotalSize += len(chunk.Data)
	}
	
	item := make([]byte, TotalSize)

	i := 0
	var DataSize int
	for _, chunk := range Chunks {
		DataSize = len(chunk.Data)
		copy(item[i:i+DataSize], chunk.Data)
		i += DataSize
	}

	return item, nil
}

func (r Reconstructor) DeleteItem(id string) {
	delete(r,id)
}
